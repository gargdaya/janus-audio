import { useEffect, useMemo, useState } from "react";
import Audiobridge from "../components/audiobridge";

const AudioRoom = () => {
  const [peers, setPeers] = useState([]);
  const [displayName, setDisplayName] = useState();
  const [selfMuted, setSelfMuted] = useState(false);
  const [token, setToken] = useState();
  const [userId, setUserId] = useState();
  useEffect(() => {
    setDisplayName("user" + Math.floor(Math.random()*10));
    setUserId(Math.floor(Math.random() * 100));
    fetch("https://zo.xyz/janus/auth/token/")
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        const { token } = data.data;

        setToken(token);
      })
      .catch(console.log);
  }, []);

  return (
    <Audiobridge
      peers={peers}
      setPeers={setPeers}
      displayName={displayName}
      setDisplayName={setDisplayName}
      roomCode={1234}
      setSelfMuted={setSelfMuted}
      selfMuted={selfMuted}
      token={token}
      userId={userId}
    />
  );
};

export default AudioRoom;
