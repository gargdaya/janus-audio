import { Routes, Route } from "react-router-dom";
import './App.css'
import { EchotestComponent } from "./components/echotest";
import AudioCallTest from './components/audioroom'
import AudioRoom from "./pages/AudioRoom";

function App() {
  return (
    <Routes>
      <Route path="/" element={<EchotestComponent />} />
      <Route path="/audio" element={<AudioCallTest />} />
      <Route path="/audioroom" element={<AudioRoom />} />
    </Routes>
  );
}

export default App;
