import { useEffect, useRef, useState } from "react";
import Janus from "../janus";
import { GoUnmute } from "react-icons/go";
import { AiOutlineAudioMuted } from "react-icons/ai";


const Audioroom = () => {
  const audioRef = useRef();
  const [usersList, setUsersList] = useState([]);
  const [room] = useState(1234);
  const [audioEnabled, setAudioEnabled] = useState(true);
  console.log(usersList, "ssss");
  const [isInitialized, setIsInitialized] = useState(false);
  const [isStarted, setIsStarted] = useState(false);
  const [username, setUsername] = useState("");
  const [audioInput, setAudioInput] = useState([]);

  const [audioOutput, setAudioOutput] = useState([]);

  const [myId, setMyId] = useState();
  let audiobridge;
  useEffect(() => {
    Janus.init({
      debug: true,
      callback: () => {
        setIsInitialized(true);
      },
    });
  }, []);
  const [audioBridge, setAudioBridge] = useState();
  const [janusc, setJanusc] = useState();
// members,mute,unmute roomId,userId
  const start = () => {
    if (!Janus.isWebrtcSupported()) {
      alert("Webrtc not supported");
      return;
    }
    const janus = new Janus({
      server: ["wss://av.io.zo.xyz/socket"],
      // token:'1658824618,janus,janus.plugin.audiobridge,janus.plugin.videoroom:raPFY315SzWF4HoiQoc0pYE44XY=',
      iceServers: [{ urls: "stun:stun.l.google.com:19302" }],
      success: () => {
        janus.attach({
          plugin: "janus.plugin.audiobridge",
          iceServers: [{ urls: "stun:stun.l.google.com:19302" }],

          success: (pluginHandle) => {
            audiobridge = pluginHandle;
            setAudioBridge(pluginHandle);
            Janus.log(
              `Plugin attached! ( ${audiobridge.getPlugin()}, id= ${audiobridge.getId()})`
            );

            const body = { request: "join", room: room, display: username };

            pluginHandle.send({ message: body });

            setIsStarted(true);
          },
          error: (error) => {
            console.log(error);
          },
          consentDialog: (on) => {
            Janus.debug(
              "Consent dialog should be " + (on ? "on" : "off") + " now"
            );
          },
          iceState: (state) => {
            Janus.log("ICE state changed to " + state);
          },
          mediaState: (medium, on) => {
            console.log(
              "Janus " +
                (on ? "started" : "stopped") +
                " receiving our " +
                medium
            );
          },
          webrtcState: (on) => {
            Janus.log(
              "Janus says our WebRTC PeerConnection is " +
                (on ? "up" : "down") +
                " now"
            );
          },
          onmessage: (msg, jsep) => {
            console.log(msg, "ghjghjhjkjghjk");
            Janus.debug(" ::: Got a message :::", msg, jsep);

            const event = msg["audiobridge"];

            Janus.debug("Event: " + event);
            if (event === "joined") {
              if (msg["id"]) {
                setMyId(msg.id);
                audiobridge.createOffer({
                  media: { video: false },
                  success: (jsep) => {
                    Janus.log("Got SDP!", jsep);
                    audiobridge.send({
                      message: { request: "configure", muted: false },
                      jsep: jsep,
                    });
                  },
                  error: (error) => {
                    console.log(error, "jjj");
                  },
                });

                (async () => {
                  const devices =
                    await navigator.mediaDevices.enumerateDevices();
                  setAudioInput(
                    devices?.filter((e) => e.kind === "audioinput")
                  );
                  setAudioOutput(
                    devices?.filter((e) => e.kind === "audiooutput")
                  );
                })();
              }
            } else if (event === "roomchanged") {
              setMyId(msg.id);
            } else if (event === "destroyed") {
              Janus.warn("The room has been destroyed!");
              window.location.reload();
            } else if (event === "event") {
              if (msg.error) {
                alert(msg.error);
              }
              if (msg.leaving) {
                console.log(`Participant left:${msg.leaving}`);
              }
            }
            if (event) {
              audiobridge.send({
                message: { request: "listparticipants", room: room },
                success: (data) => setUsersList(data.participants),
              });
            }

            if (jsep) {
              Janus.debug("Handling SDP as well...", jsep);
              audiobridge.handleRemoteJsep({ jsep: jsep });
            }
          },
          onlocaltrack: (track, on) => {},
          onremotetrack: (track, on) => {
            console.log(
              "audioRef.current?.srcObject",
              audioRef.current?.srcObject
            );

            if (audioRef.current?.srcObject || track.kind !== "audio") {
              return;
            }
            if (!on) {
              if (audioRef.current.srcObject) {
                try {
                  const tracks = audioRef.current.srcObject.getTracks();
                  tracks.map((e) => (e ? e.stop : ""));
                } catch (error) {
                  console.log(error);
                }
              }
              audioRef.current.srcObject = null;
            }
            Janus.debug(
              "Remote track " + (on ? "added" : "removed") + ":",
              track
            );
            Janus.log(
              "Remote track " + (on ? "added" : "removed") + ":",
              track
            );
            audioRef.current.srcObject = new MediaStream([track]);
          },
          oncleanup: () => {
            setUsersList([]);
            audioRef.current = undefined;
            setIsStarted(false);
            setUsername(undefined);
          },
        });
      },
      error: (error) => {
        console.log(error);
      },
      destroyed: () => {
        window.location.reload();
      },
    });
    setJanusc(janus);
  };
  const [inputAudio, setInputAudio] = useState();
  const [outputAudio, setOutputAudio] = useState();
  const changeOutput = () => {
    if (outputAudio) {
      audioRef.current
        .setSinkId(outputAudio)
        .then((d) => console.log("devices", d))
        .catch((e) => console.log("devices", e));
    }
  };
  useEffect(() => {
    if (isStarted && isInitialized) {
      navigator.mediaDevices.ondevicechange = (event) => {
        (async () => {
          const devices = await navigator.mediaDevices.enumerateDevices();
          setAudioInput(devices?.filter((e) => e.kind === "audioinput"));
          setAudioOutput(devices?.filter((e) => e.kind === "audiooutput"));
        })();
      };
    }
  }, [isInitialized, isStarted]);

  const changeInputAudioDevice = () => {
    if (isStarted && isInitialized && audioBridge && audioInput) {
      audioBridge.createOffer({
        media: {
          video: false,
          audio: { deviceId: inputAudio },
          replaceAudio: true,
        },
        success: (jsep) => {
          Janus.log("Got SDP!", jsep);
          audioBridge.send({
            message: { request: "configure", muted: !audioEnabled },
            jsep: jsep,
          });
        },
        error: (error) => {
          console.log(error, "jjj");
        },
      });
    }
  };

  const [listRooms, setListRooms] = useState([]);
  console.log("🚀 ~ file: audioroom.js ~ line 238 ~ Audioroom ~ listRooms", listRooms)

  useEffect(() => {
    if (isStarted && isInitialized && audioBridge) {
      audioBridge?.send({
        message: { request: "list" },
        success: (data) => setListRooms(data.list),
      });
    }
  }, [audioBridge, isInitialized, isStarted, room, audioEnabled]);
  const darkThemeMq = window.matchMedia("(prefers-color-scheme: dark)");

  return (
    <div
      className={`bg-gradient-to-tl ${
        darkThemeMq?.matches
          ? "bg-black text-white"
          : "bg-gray-100 text-gray-900"
      }`}
    >
      {/* <div className="py-2 px-4 flex">
        <img alt="logo" src={Logo} width={64} />
        <h1 className="mt-2 font-bold text-lg">Audio Commonroom</h1>
      </div>
      <div className="border-b-2 " /> */}
      <div className=" flex">
        <div className=" basis-1/6 w-full flex flex-col overflow-scroll	overflow-x-auto	h-screen">
          <div className="mt-3 mx-auto pb-3 border-b-2 w-full px-2">
            <h1 className="text-md text-red-700">Audio Rooms</h1>
          </div>
        </div>
        <div className="basis-auto  w-full p-2">
          <div className="flex justify-center items-center">
            {!isStarted && (
              <>
                <input
                  className="border-2 p-2 rounded-sm text-black"
                  type="text"
                  placeholder="Enter Name"
                  value={username}
                  onChange={(e) => setUsername(e.target.value)}
                />
              </>
            )}
            <button
              className="bg-blue-500 text-white p-2 m-3 rounded-lg"
              onClick={() =>
                username && !isStarted ? start() : janusc?.destroy()
              }
            >
              {isStarted ? "Stop" : "Join The Room"}
            </button>
          </div>
          <audio hidden autoPlay playsInline ref={audioRef} />
          {isInitialized && isStarted && (
            <>
              <div className="flex flex-row items-center w-1/2 mx-auto justify-center m-3 border space-x-2">
                <div className=" flex flex-col p-3">
                  <div className="text-gray-500">Audio Input</div>
                  <select
                    className="border text-black"
                    onChange={(e) => setInputAudio(e.target.value)}
                  >
                    {audioInput?.map((el) => (
                      <option key={el.deviceId + el.kind} value={el.deviceId}>
                        {el.label}
                      </option>
                    ))}
                  </select>
                </div>
                <button
                  className="p-2 text-blue-500"
                  onClick={changeInputAudioDevice}
                >
                  Change Device
                </button>
              </div>

              <div className=" mx-auto border-2 border-purple-400 w-1/3">
                <h1 className="text-xl m-3">Users List</h1>
                <ul className="list-unstyled">
                  {usersList.map((el) => (
                    <li
                      className="border p-1 flex flex-row justify-between items-center"
                      key={el.id}
                    >
                      <div>{el.display}</div>
                      <div className="">
                        {el.talking && !el.muted ? <GoUnmute /> : ""}
                      </div>

                      {myId === el.id ? (
                        <div
                          onClick={() => {
                            setAudioEnabled(!audioEnabled);
                            // setMyId(el.id)
                            audioBridge?.send({
                              message: {
                                request: "configure",
                                muted: audioEnabled,
                              },
                            });
                          }}
                          className="cursor-pointer"
                        >
                          {audioEnabled ? "mute" : "unmute"}
                        </div>
                      ) : (
                        <div className="">
                          {el.muted ? <AiOutlineAudioMuted /> : ""}
                        </div>
                      )}
                    </li>
                  ))}
                </ul>
              </div>
              <div className="flex flex-row p-3 w-1/3 mx-auto ">
                <select
                  className="border text-black"
                  onChange={(e) => setOutputAudio(e.target.value)}
                >
                  {audioOutput?.map((el) => (
                    <option key={el.deviceId + el.kind} value={el.deviceId}>
                      {el.label}
                    </option>
                  ))}
                </select>
                <button className="p-2 text-blue-500" onClick={changeOutput}>
                  Change Output
                </button>
              </div>
            </>
          )}
        </div>
      </div>
    </div>
  );
};

export default Audioroom;
