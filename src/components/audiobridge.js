import { useEffect, useRef, useState } from "react";
import Janus from "../janus";
import { GoUnmute } from "react-icons/go";
import { AiOutlineAudioMuted } from "react-icons/ai";

const Audiobridge = ({
  peers,
  setPeers,
  token,
  userId,
  roomCode,
  displayName,
  setDisplayName,
  selfMuted = false,
  setSelfMuted,
}) => {
  console.log("🚀 ~ file: audiobridge.js ~ line 17 ~ peers", peers);
  const [isJanusInitialized, setIsJanusInitialized] = useState(false);
  const darkThemeMq = window.matchMedia("(prefers-color-scheme: dark)");
  const [audioBridgePlugin, setAudioBridgePlugin] = useState();
  const [isStarted, setIsStarted] = useState(false);
  const audioRef = useRef();
  const [janusInstance, setJanusInstance] = useState();
  const [errors, setErrors] = useState();
  useEffect(() => {
    if (errors) {
      console.log("🚀 ~ file: audiobridge.js ~ line 27 ~ useEffect ~ errors", errors)
      alert(errors.message);
    }
  }, [errors]);

  useEffect(() => {
    let audiobridge;
    Janus.init({
      debug: true,
      callback: () => {
        setIsJanusInitialized(true);
        if (!Janus.isWebrtcSupported()) {
          alert("Webrtc not supported");
          return;
        }
        if (userId && token) {
          const janus = new Janus({
            server: ["wss://av.io.zo.xyz/socket"],
            token: token,
            iceServers: [{ urls: "stun:stun.l.google.com:19302" }],
            success: () => {
              janus.attach({
                plugin: "janus.plugin.audiobridge",
                iceServers: [{ urls: "stun:stun.l.google.com:19302" }],

                success: (pluginHandle) => {
                  audiobridge = pluginHandle;
                  setAudioBridgePlugin(pluginHandle);

                  const body = {
                    request: "join",
                    room: roomCode,
                    display: displayName,
                    id: userId,
                  };
                  pluginHandle.send({ message: body });

                  Janus.log(
                    `Plugin attached! ( ${audiobridge.getPlugin()}, id= ${audiobridge.getId()})`
                  );
                },
                error: (error) => {
                  console.log(error, "hhhh");
                  setErrors(error);
                },
                consentDialog: (on) => {
                  Janus.debug(
                    "Consent dialog should be " + (on ? "on" : "off") + " now"
                  );
                },
                iceState: (state) => {
                  Janus.log("ICE state changed to " + state);
                },
                mediaState: (medium, on) => {
                  console.log(
                    "Janus " +
                      (on ? "started" : "stopped") +
                      " receiving our " +
                      medium
                  );
                },
                webrtcState: (on) => {
                  Janus.log(
                    "Janus says our WebRTC PeerConnection is " +
                      (on ? "up" : "down") +
                      " now"
                  );
                },
                onmessage: (msg, jsep) => {
                  console.log(msg, "ghjghjhjkjghjk");
                  Janus.debug(" ::: Got a message :::", msg, jsep);

                  const event = msg["audiobridge"];

                  Janus.debug("Event: " + event);
                  if (event === "joined") {
                    if (msg["id"]) {
                      //   setMyId(msg.id);

                      audiobridge.createOffer({
                        media: { video: false },
                        success: (jsep) => {
                          Janus.log("Got SDP!", jsep);
                          audiobridge.send({
                            message: { request: "configure", muted: false },
                            jsep: jsep,
                          });
                          setIsStarted(true);
                        },
                        error: (error) => {
                          console.log(error, "jjj");
                          setErrors(error);
                        },
                      });

                      // (async () => {
                      //   const devices =
                      //     await navigator.mediaDevices.enumerateDevices();
                      //   setAudioInput(
                      //     devices?.filter((e) => e.kind === "audioinput")
                      //   );
                      //   setAudioOutput(
                      //     devices?.filter((e) => e.kind === "audiooutput")
                      //   );
                      // })();
                    }
                  } else if (event === "roomchanged") {
                    //   setMyId(msg.id);
                  } else if (event === "destroyed") {
                    Janus.warn("The room has been destroyed!");
                    window.location.reload();
                  } else if (event === "event") {
                    if (msg.error) {
                      alert(msg.error);
                    }
                    if (msg.leaving) {
                      console.log(`Participant left:${msg.leaving}`);
                    }
                  }
                  if (event) {
                    audiobridge.send({
                      message: { request: "listparticipants", room: roomCode },
                      success: (data) => setPeers(data.participants),
                    });
                  }

                  if (jsep) {
                    Janus.debug("Handling SDP as well...", jsep);
                    audiobridge.handleRemoteJsep({ jsep: jsep });
                  }
                },
                onlocaltrack: (track, on) => {},
                onremotetrack: (track, on) => {
                  console.log(
                    "audioRef.current?.srcObject",
                    audioRef.current?.srcObject
                  );

                  if (audioRef.current?.srcObject || track.kind !== "audio") {
                    return;
                  }
                  if (!on) {
                    if (audioRef.current.srcObject) {
                      try {
                        const tracks = audioRef.current.srcObject.getTracks();
                        tracks.map((e) => (e ? e.stop : ""));
                      } catch (error) {
                        console.log(error);
                      }
                    }
                    audioRef.current.srcObject = null;
                  }
                  Janus.debug(
                    "Remote track " + (on ? "added" : "removed") + ":",
                    track
                  );
                  Janus.log(
                    "Remote track " + (on ? "added" : "removed") + ":",
                    track
                  );
                  audioRef.current.srcObject = new MediaStream([track]);
                },
                oncleanup: () => {
                  setPeers([]);
                  audioRef.current = undefined;
                  setIsStarted(false);
                  setDisplayName(undefined);
                },
              });
            },
            error: (error) => {
              console.log(error);
            },
            destroyed: () => {
              window.location.reload();
            },
          });
          setJanusInstance(janus);
        }
      },
    });
  }, [displayName, roomCode, setDisplayName, setPeers, token, userId]);

  return (
    <div
      className={`bg-gradient-to-tl ${
        darkThemeMq?.matches
          ? "bg-black text-white"
          : "bg-gray-100 text-gray-900"
      }`}
    >
      {/* <div className="py-2 px-4 flex">
        <img alt="logo" src={Logo} width={64} />
        <h1 className="mt-2 font-bold text-lg">Audio Commonroom</h1>
      </div>
      <div className="border-b-2 " /> */}
      <div className=" flex">
        <div className=" basis-1/6 w-full flex flex-col overflow-scroll	overflow-x-auto	h-screen">
          <div className="mt-3 mx-auto pb-3 border-b-2 w-full px-2">
            <h1 className="text-md text-red-700">Audio Rooms</h1>
          </div>
          <button
            className="bg-blue-500 text-white p-2 m-3 rounded-lg"
            onClick={() => janusInstance?.destroy()}
          >
            Leave
          </button>
        </div>
        <div className="basis-auto  w-full p-2">
          <audio hidden autoPlay playsInline ref={audioRef} />
          {isJanusInitialized && isStarted && !errors && (
            <>
              <div className=" mx-auto border-2 border-purple-400 w-1/3">
                <h1 className="text-xl m-3">Users List</h1>
                <ul className="list-unstyled">
                  {peers.map((el) => (
                    <li
                      className="border p-1 flex flex-row justify-between items-center"
                      key={el.id}
                    >
                      <div>{el.display}</div>
                      <div className="">
                        {el.talking && !el.muted ? <GoUnmute /> : ""}
                      </div>

                      {userId === el.id ? (
                        <div
                          onClick={() => {
                            setSelfMuted(!el.muted);
                            // setMyId(el.id)
                            audioBridgePlugin?.send({
                              message: {
                                request: "configure",
                                muted: !el.muted,
                              },
                            });
                          }}
                          className="cursor-pointer"
                        >
                          {selfMuted ? "unmute" : "mute"}
                        </div>
                      ) : (
                        <div className="">
                          {el.muted ? <AiOutlineAudioMuted /> : ""}
                        </div>
                      )}
                    </li>
                  ))}
                </ul>
              </div>
            </>
          )}
        </div>
      </div>
    </div>
  );
};

export default Audiobridge;
