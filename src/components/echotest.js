import React, { useRef, useEffect, useState } from "react";
import Janus from "../janus";

export const EchotestComponent = () => {
  const myVideo = useRef();
  const remoteVideo = useRef();
  const [isInitialized, setIsInitialized] = useState(false);
  const remoteAudio = useRef();
  const [started, setStarted] = useState(false);
  useEffect(() => {
    Janus.init({
      debug: true,
      // dependencies:Janus.useDefaultDependencies(),
      callback: () => {
        console.log("done");
        setIsInitialized(true);
      },
    });
  }, []);

  let echotest;
  const [janusC, setJanusC] = useState()
  const start = () => {
    if (isInitialized) {
     const janus = new Janus({
        server: "http://localhost:8088/janus",
        iceServers: [{ urls: "stun:stun.l.google.com:19302" }],
        success: () => {
          janus.attach({
            plugin: "janus.plugin.echotest",
            opaqueId: Janus.randomString(12),
            success: (pluginHandle) => {
              echotest = pluginHandle;
              console.log(
                "🚀 ~ file: App.js ~ line 21 ~ useEffect ~ pluginHandle",
                pluginHandle
              );
              echotest.send({ message: { audio: true, video: true } });
              pluginHandle.createOffer({
                success: (jsep) => {
                  pluginHandle.send({
                    message: { audio: true, video: true },
                    jsep: jsep,
                  });
                },
              });
              setStarted(true);
            },
            error: (error) => {
              console.error(error);
              alert(error);
            },
            consentDialog: (on) => {
              Janus.debug(
                "Consent dialog should be " + (on ? "on" : "off") + " now"
              );
            },
            iceState: (state) => {
              Janus.log("ICE state changed to " + state);
            },
            mediaState: (medium, on, mid) => {
              Janus.log(
                "Janus " +
                  (on ? "started" : "stopped") +
                  " receiving our " +
                  medium +
                  " (mid=" +
                  mid +
                  ")"
              );
            },
            webrtcState: (on) => {
              Janus.log(
                "Janus says our WebRTC PeerConnection is " +
                  (on ? "up" : "down") +
                  " now"
              );
            },
            onlocaltrack: function (track, on) {
              console.log("🚀 ~ file: App.js ~ line 76 ~ start ~ on", on);
              Janus.debug(
                "Local track " + (on ? "added" : "removed") + ":",
                track
              );
              if (track.kind === "video") {
                const stream = new MediaStream([track]);

                myVideo.current.srcObject = stream;
              }
            },
            onmessage: (msg, jsep) => {
              console.log("line89", msg);
              if (jsep) {
                echotest.handleRemoteJsep({ jsep: jsep });
              }
            },
            onremotetrack: function (track, on) {
              console.log("🚀 ~ file: App.js ~ line 97 ~ start ~ on", on);
              console.log("🚀 ~ file: App.js ~ line 97 ~ start ~ track", track);
              if (track.kind === "video") {
                const stream = new MediaStream([track]);
                remoteVideo.current.srcObject = stream;
              } else if (track.kind === "audio") {
                const stream = new MediaStream([track]);
                remoteAudio.current.srcObject = stream;
              }
            },
            oncleanup: () => {
              myVideo.current.hidden = true;
              remoteVideo.current.hidden = true;
            },
          });
        },
        error: (error) => {
          console.log(error);
        },
        destroyed: () => {
          window.location.reload();
        },
      });
	  setJanusC(janus)
    }
  };

  return (
    <div>
      <button onClick={() => (started ? janusC?.destroy() : start())}>
        {started ? "Stop" : "Start"}
      </button>
      <audio hidden autoPlay playsInline ref={remoteAudio} />
      <p>
        <video playsInline autoPlay muted ref={myVideo} style={{marginRight:"10px"}} />
     
        <video playsInline autoPlay ref={remoteVideo} />
      </p>
    </div>
  );
};

export default EchotestComponent;
